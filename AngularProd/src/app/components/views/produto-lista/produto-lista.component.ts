import { ProdutoServiceService } from './../../services/produto-service.service';
import { MatTable } from '@angular/material/table';
import { FormularioDialogComponent } from './../formulario-dialog/formulario-dialog.component';
import { Produto } from './../../models/Produto';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';



@Component({
  selector: 'app-produto-lista',
  templateUrl: './produto-lista.component.html',
  styleUrls: ['./produto-lista.component.scss']
})

export class ProdutoListaComponent implements OnInit {
  @ViewChild(MatTable)
  table!: MatTable<any>
  displayedColumns: string[] = ['id', 'name', 'price', 'sku', 'actions'];
  dataSource: Produto[] = [];

  constructor(
    private dialog: MatDialog,
    private prodService: ProdutoServiceService,
  ) { }

  ngOnInit(): void {
    this.getProdutos();
  }

  getProdutos() {
    this.prodService.getProduto()
      .subscribe(
        (res) => {
          this.dataSource = res
        },
        (err) => {
          console.log(err)
        }
      )
  }

  addDialog(produto: Produto | null) {
    const dialogRef = this.dialog.open(FormularioDialogComponent, {
      width: '300px',
      data: produto === null ? {
        _id: '',
        name: '',
        price: 0,
        sku: ''

      } : {
        id: produto._id,
        name: produto.name,
        price: produto.price,
        sku: produto.sku
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.prodService.postProduto(result)
          .subscribe(
            (res) => {
              this.dataSource.push(res)
              this.getProdutos();
            },
            (err) => {
              console.log(err)
            }
          )
      }
    });

  }

  delete(id: any) {
    this.prodService.delete(id).subscribe(
      () => {
        let i = this.dataSource.findIndex(prod => id.id == prod._id);
        if (i >= 0) {
          this.dataSource.splice(i, 1);
        }
        this.getProdutos();
      },
      (err) => {
        console.log(err);
      }

    )

  }
}
