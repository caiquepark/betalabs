export interface Produto {
    name: string;
    price: number;
    sku: string;
    _id?: string;
    
}