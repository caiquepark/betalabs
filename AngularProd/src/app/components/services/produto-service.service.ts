import { Produto } from './../models/Produto';
import { catchError, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EMPTY, Observable, pipe } from 'rxjs';
import { ToastrService } from 'ngx-toastr';


@Injectable({
  providedIn: 'root'
})
export class ProdutoServiceService {


  readonly url = 'http://angular-test.blabs.us';

  constructor(
    private http: HttpClient,
    private toastr: ToastrService
     ) { }


  getProduto(): Observable<any> {
    return this.http.get<any>(this.url)
  }

  postProduto(prod: Produto): Observable<Produto> {
    return this.http.post<Produto>(this.url, prod).pipe(
      map( res => this.exibirSucesso(res)),
      catchError(err => this.exibirErro(err))
    );
    
  }

  delete(id: any): Observable<any> {
    return this.http.delete<any>(`${this.url}/?id=${id}`).pipe(
    map( res => this.exibirSucesso(res)),
    catchError(err => this.exibirErro(err))
  );
 }

exibirErro(e: any): Observable<any> {
  this.exibirMessagem( 'ERRO!!', 'Não foi possivel realizar a operação',  'toast-error');
  return EMPTY;
}

exibirSucesso(res: any): Observable<any> {
  this.exibirMessagem('Sucesso', 'Operação realizado com sucesso', 'toast-success');
  return EMPTY;
}

exibirMessagem(titulo: string, mensagem: string, tipo: string): void {
  this.toastr.show(mensagem, titulo, {closeButton: true, progressBar: true}, tipo);
}

}
